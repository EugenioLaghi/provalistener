package com.prova.bo;

import com.prova.api.StockItem;

public interface StockBo {

	String save(String id, String code, String name);
	String update(String id, String code, String name);
	StockItem findById(String id);
	String delete(String id);
	
}
