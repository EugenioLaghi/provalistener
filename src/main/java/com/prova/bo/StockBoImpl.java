package com.prova.bo;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.prova.api.StockItem;
import com.prova.dao.StockDao;
import com.prova.response.SessionItem;

@Service(value="stockBoDelegate")
public class StockBoImpl implements StockBo {

	@Resource(name="stockDaoDelegate")
	StockDao stockDao;
	
	@Resource(name="sessionItem")
	SessionItem sessionItem;
	
	@Override
	public StockItem findById(String id) {
		sessionItem.setParam("2");
		System.out.println("@@@@@@@@@@@@@@@@ execution ininin @@@@@@@@@@@@@@@@@@@@@@@");

		return stockDao.findByStockCode(id);
	}

	@Override
	public String save(String id, String code, String name) {
		
		StockItem stockItem = new StockItem(id, code, name);
		
		return stockDao.save(stockItem);
	}

	@Override
	public String update(String id, String code, String name) {
		
		StockItem stockItem = new StockItem(id, code, name);
		
		return stockDao.update(stockItem);
	}

	@Override
	public String delete(String id) {
		return stockDao.delete(id);
	}

}
