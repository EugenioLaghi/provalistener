package com.prova.dao;

import com.prova.api.DeptItem;
import com.prova.exceptions.ProvaException;

public interface DeptDao {
	
		public void insert(DeptItem item);
		public DeptItem selectById(String id) throws ProvaException;
	
}
