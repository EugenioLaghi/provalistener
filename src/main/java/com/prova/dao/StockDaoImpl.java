package com.prova.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.prova.api.StockItem;
import com.prova.persistence.HibernateUtil;

@Service(value = "stockDaoDelegate")
public class StockDaoImpl implements StockDao {

	@Override
	public String save(StockItem stock) {

		Session session = null;
		
		try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (HibernateException e){
			session = HibernateUtil.getSessionFactory().openSession();			
		}

		session.beginTransaction();
        
        session.save(stock);
        
        session.getTransaction().commit();

        return "record inserted";
	        
	}

	@Override
	public String update(StockItem stock) {
		
		Session session = null;
		
		try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (HibernateException e){
			session = HibernateUtil.getSessionFactory().openSession();			
		}
        
		session.beginTransaction();
        
        session.update(stock);
        
        session.getTransaction().commit();

        return "record updated";
	}

	@Override
	public String delete(String id) {
		
		Session session = null;
		
		try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (HibernateException e){
			session = HibernateUtil.getSessionFactory().openSession();			
		}
        
		session.beginTransaction();
        
        Criteria criteria = session.createCriteria(StockItem.class);
        
        criteria.add(Restrictions.idEq(id));
        
        session.delete(criteria.uniqueResult());

        session.getTransaction().commit();

        return "record deleted";

		
	}

	@Override
	public StockItem findByStockCode(String id) {
		Session session = null;
		
		try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (HibernateException e){
			session = HibernateUtil.getSessionFactory().openSession();			
		}

		Criteria criteria = session.createCriteria(StockItem.class);
        
        criteria.add(Restrictions.idEq(id));
        
        return (StockItem)criteria.uniqueResult();

	}

}
