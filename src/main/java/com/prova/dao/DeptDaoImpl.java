package com.prova.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.prova.api.DeptItem;
import com.prova.exceptions.ProvaException;

public class DeptDaoImpl implements DeptDao {

	private DataSource dataSource;
	 
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public void insert(DeptItem item) {
		
		String sql = "INSERT INTO DEPT " +
				"(ID, CITY, NAME) VALUES (?, ?, ?)";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, item.getId());
			ps.setString(2, item.getCity());
			ps.setString(3, item.getName());
			ps.executeUpdate();
			ps.close();
 
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}

	}

	@Override
	public DeptItem selectById(String id) throws ProvaException {
		String sql = "SELECT * FROM DEPT WHERE ID = ?";
		 
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			DeptItem item = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				item = new DeptItem(rs.getString("ID"),rs.getString("CITY"),rs.getString("NAME"));
			} else {
				throw new ProvaException("1","No records found");
			}
			rs.close();
			ps.close();
			return item;
		} catch (SQLException e) {
			throw new ProvaException(String.valueOf(e.getErrorCode()),e.getMessage());
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
	}

}
