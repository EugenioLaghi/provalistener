package com.prova.dao;

import com.prova.api.StockItem;

public interface StockDao {

	String save(StockItem stock);
	String update(StockItem stock);
	String delete(String id);
	StockItem findByStockCode(String stockCode);
	
}

