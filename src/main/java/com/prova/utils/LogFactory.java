package com.prova.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class LogFactory {
	
	public void init(){
		
//		DOMConfigurator.configure("log3j.xml");
		
	}
	
	public static Logger getLogger(Class<?> clazz) {
		
		DOMConfigurator.configure("log4j.xml");
		
		return Logger.getLogger(clazz);
		
	}
	
}