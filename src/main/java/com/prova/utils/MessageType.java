package com.prova.utils;

public enum MessageType {
	
	M1("001"),
	
	M2("002");
	
	private final String value;

	MessageType(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static MessageType fromValue(String v) {
		for (MessageType c : MessageType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
