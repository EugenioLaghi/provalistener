package com.prova.utils;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


@Service(value="translator")
public class ListenerTranslator {
	
    //get Logger Instance
	private static final Logger log = LogFactory.getLogger(ListenerTranslator.class);
	
	public Map<String,String> translate(String message){

		Map<String,String> out = new LinkedHashMap<String, String>();
		
		try{
			
			MessageType type = MessageType.fromValue(message.substring(0, 3));
			
			message = message.substring(3);
			
			switch(type){
			case M1 :
				out.put("campo1", message.substring(0, 5));
				message = message.substring(5);
				out.put("campo2", message.substring(0, 6));
				break;
			case M2 :
				out.put("primoCampo", message.substring(0, 4));
				message = message.substring(4);
				out.put("secondoCampo", message.substring(0, 7));
				break;
			default :
				out.put("messaggio", message);
				break;
			}
			
			return out;
			
		} catch (Exception e) {
			
			out.put("errore",e.getMessage());
			
			log.error("Errore durante la traduzione", e);
			
			return out;
			
		}
	}
}
