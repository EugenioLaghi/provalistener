package com.prova.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class Log4Listener {

	public static Logger getLogger(Class<?> clazz) {
		
		// load configuration File in XML format
		DOMConfigurator.configure("myLog.xml");
		
		// get Logger Instance
		return Logger.getLogger(clazz);
		
	}
	
	public static void main(String[] args) {
		
		DOMConfigurator.configure("myLog.xml");
		
//		Logger log = Logger.get
		
	}
	
}
