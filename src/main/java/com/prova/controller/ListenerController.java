package com.prova.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prova.service.ListenerService;

@Controller
@RequestMapping("/Listener")
public class ListenerController {

	@Resource(name="listenerService")
	ListenerService listener;
	
	@RequestMapping("/ping")
	public @ResponseBody
	String ping(){
		return "OK";
	}
	
	@RequestMapping(value={"","/"})
	public @ResponseBody
	String listener(HttpServletRequest request) throws Exception{
		return listener.listen();
	}
	
	@RequestMapping("/write")
	public @ResponseBody
	String listenerWriter(HttpServletRequest request) throws Exception{
		listener.listenAndWrite();
		return "DONE";
	}
	
}
