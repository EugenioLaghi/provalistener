package com.prova.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Redirection")
public class RedirectionController {
	
	@RequestMapping("/ping")
	public @ResponseBody
	String ping(){
		return "OK";
	}

	@RequestMapping(value="/")
	public @ResponseBody Object redirection(HttpServletRequest request){
		return new ModelAndView("../../jsp/prova.html","model","prova");
	}
	
	
}
