package com.prova.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.prova.service.LogServiceImpl;

@Controller
@RequestMapping("/Log4j")
public class Log4jController {
	
	@Autowired
	LogServiceImpl service;
	
	@RequestMapping("/ping")
	public @ResponseBody
	String ping(){
		return "OK";
	}

	@RequestMapping(value={"","/"}, method = RequestMethod.GET)
	public @ResponseBody String log(@RequestParam(value="id") String id,HttpServletRequest request){
		return service.writeToLog(id);
	}
	
}
