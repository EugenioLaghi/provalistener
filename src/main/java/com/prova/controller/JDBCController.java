package com.prova.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prova.api.DeptItem;
import com.prova.exceptions.ProvaException;
import com.prova.response.ProvaResponse;
import com.prova.service.DeptServiceImpl;

@Controller
@RequestMapping("/DB")
public class JDBCController {

	@Resource(name="")
	DeptServiceImpl deptService;
	
	@RequestMapping("/ping")
	public @ResponseBody
	String ping(){
		return "OK";
	}
	
	@RequestMapping(value="/read",method = RequestMethod.GET)
	public @ResponseBody
	ProvaResponse<DeptItem> select(@RequestParam(value="id") String id, HttpServletRequest request) throws Exception{
		ProvaResponse<DeptItem> out = new ProvaResponse<DeptItem>();
		try{
			out.setItem(deptService.getDept(id));
		} catch (ProvaException e){
			out.setError(e.getLocalizedMessage());
		} catch (Exception e){
			out.setNotHandledError(e.getMessage());
		}
		
		return out;
	}
	
	@RequestMapping(value="/write",method = RequestMethod.POST)
	public @ResponseBody
	String insert(@ModelAttribute DeptItem item, HttpServletRequest request) throws Exception{
		return deptService.addDept(item);
	}
	
}
