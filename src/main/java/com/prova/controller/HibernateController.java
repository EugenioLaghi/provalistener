package com.prova.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prova.api.StockItem;
//import com.prova.aspect.AspectProva;
import com.prova.bo.StockBo;
import com.prova.exceptions.ProvaException;
import com.prova.response.ProvaResponse;
import com.prova.response.SessionItem;

@Controller
@RequestMapping("/Hibernate")
public class HibernateController {

	@Resource(name="")
	StockBo stockBo;
	
	@Resource(name="sessionItem")
	SessionItem sessionItem;

	@RequestMapping("/ping")
	public @ResponseBody
	String ping(){
		return "OK";
	}
	
	@RequestMapping(value="/read",method = RequestMethod.GET)
	public @ResponseBody
	ProvaResponse<StockItem> select(@RequestParam(value="id") String id, HttpServletRequest request) throws Exception{
		ProvaResponse<StockItem> out = new ProvaResponse<StockItem>();
		try{
			StockItem response = stockBo.findById(id);
			out.setItem(response);
			out.setParam(sessionItem.getParam());
		} catch (ProvaException e){
			out.setError(e.getLocalizedMessage());
		} catch (Exception e){
			out.setNotHandledError(e.getMessage());
		}
		
		return out;
	}
	
	@RequestMapping(value="/create",method = RequestMethod.GET)
	public @ResponseBody
	ProvaResponse<String> insert(@RequestParam(value="id") String id,
			@RequestParam(value="code") String code,
			@RequestParam(value="name") String name,
			HttpServletRequest request) throws Exception{
		
		ProvaResponse<String> out = new ProvaResponse<String>();
		
		try{
			String save = stockBo.save(id,code,name);
			out.setItem(save);
		} catch (ProvaException e){
			out.setError(e.getLocalizedMessage());
		} catch (Exception e){
			out.setNotHandledError(e.getMessage());
		}

		return out;
	}
	
	@RequestMapping(value="/update",method = RequestMethod.GET)
	public @ResponseBody
	ProvaResponse<String> update(@RequestParam(value="id") String id,
			@RequestParam(value="code") String code,
			@RequestParam(value="name") String name,
			HttpServletRequest request) throws Exception{
		
		ProvaResponse<String> out = new ProvaResponse<String>();
		
		try{
			String update = stockBo.update(id,code,name);
			out.setItem(update);
		} catch (ProvaException e){
			out.setError(e.getLocalizedMessage());
		} catch (Exception e){
			out.setNotHandledError(e.getMessage());
		}

		return out;
	}
	
	@RequestMapping(value="/delete",method = RequestMethod.GET)
	public @ResponseBody
	ProvaResponse<String> delete(@RequestParam(value="id") String id,
			HttpServletRequest request) throws Exception{
		
		ProvaResponse<String> out = new ProvaResponse<String>();
		
		try{
			String delete = stockBo.delete(id);
			out.setItem(delete);
		} catch (ProvaException e){
			out.setError(e.getLocalizedMessage());
		} catch (Exception e){
			out.setNotHandledError(e.getMessage());
		}

		return out;
	}
	
}
