package com.prova.exceptions;

public class ProvaException extends RuntimeException {

	private String errorCode = "";
	
	public ProvaException(String errorCode, String message){
		super(message);
		this.setErrorCode(errorCode);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
