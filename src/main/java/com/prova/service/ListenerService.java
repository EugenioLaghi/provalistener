package com.prova.service;

import java.util.Map;

import javax.naming.InitialContext;

import javax.jms.Queue;
import javax.jms.QueueSender;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.QueueSession;
import javax.jms.QueueReceiver;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prova.utils.ListenerTranslator;

@Service(value = "listenerService")
public class ListenerService {

	@Autowired
	ListenerTranslator translator;
	
	public static String newline = System.getProperty("line.separator");
	
	public String listen() throws Exception {
		
		// get the initial context
		InitialContext ctx = new InitialContext();

		// lookup the queue object
		Queue queue = (Queue) ctx.lookup("jms/ListenerJMSQueue");

		// lookup the queue connection factory
		QueueConnectionFactory connFactory = (QueueConnectionFactory) ctx.lookup("jms/ListenerJMSConnectionFactory");

		// create a queue connection
		QueueConnection queueConn = connFactory.createQueueConnection();

		// create a queue session
		QueueSession queueSession = queueConn.createQueueSession(false,Session.AUTO_ACKNOWLEDGE);

		// create a queue receiver
		QueueReceiver queueReceiver = queueSession.createReceiver(queue);

		// start the connection
		queueConn.start();

		// receive a message
		TextMessage message = (TextMessage) queueReceiver.receive();

		// print the message
//		System.out.println("received: " + message.getText());

		// close the queue connection
		queueConn.close();
		
		String out = "received: " + message.getText() + newline;
		
		Map<String,String> result = translator.translate(message.getText());
		
		if(result!=null)
			for (String key : result.keySet()){
				out += key + " : " + result.get(key) + newline; 
			}
		
		return out;
	}
	
	
	public void listenAndWrite() throws Exception {
		
		int i = 0;
		
		while(i<2){
		
		// get the initial context
		InitialContext ctx = new InitialContext();

		// lookup the queue object
		Queue queue = (Queue) ctx.lookup("jms/ListenerJMSQueue");

		// lookup the queue connection factory
		QueueConnectionFactory connFactory = (QueueConnectionFactory) ctx.lookup("jms/ListenerJMSConnectionFactory");

		// create a queue connection
		QueueConnection queueConn = connFactory.createQueueConnection();

		// create a queue session
		QueueSession queueSession = queueConn.createQueueSession(false,Session.AUTO_ACKNOWLEDGE);

		// create a queue receiver
		QueueReceiver queueReceiver = queueSession.createReceiver(queue);

		// start the connection
		queueConn.start();

		// receive a message
		TextMessage message = (TextMessage) queueReceiver.receive();

		// print the message
//		System.out.println("received: " + message.getText());

		// close the queue connection
		queueConn.close();
		
		Map<String,String> result = translator.translate(message.getText());
		
		// lookup the queue object
		Queue queue2 = (Queue) ctx.lookup("jms/ListenerReceiver");

		// lookup the queue connection factory
		QueueConnectionFactory connFactory2 = (QueueConnectionFactory) ctx.lookup("jms/ListenerJMSConnectionFactory");

		// create a queue connection
		QueueConnection queueConn2 = connFactory2.createQueueConnection();

		// create a queue session
		QueueSession queueSession2 = queueConn2.createQueueSession(false,Session.AUTO_ACKNOWLEDGE);
		
		QueueSender queueSender2 = queueSession2.createSender(queue2);
		
		// start the connection
		queueConn2.start();

		TextMessage textMessage = queueSession2.createTextMessage();
		
		String out = "";
		
		if(result!=null)
			for(String key : result.keySet()){
				out += key + " : " + result.get(key) + System.getProperty("line.separator");
			}
		
		textMessage.setText(out);
		
		queueSender2.send(textMessage);
		
//		MapMessage message2 = queueSession2.createMapMessage();
//		
//		message2.setString("input", message.getText());
//		
//		for(String key : result.keySet()){
//			message2.setString(key, result.get(key));
//		}
//
//		queueSender2.send(message2);
		
		queueConn2.close();
		
		// receive a message
//		TextMessage message2 = (TextMessage) out;
		
		i++;
		
		}
	}
	
}
