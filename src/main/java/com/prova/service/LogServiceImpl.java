package com.prova.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("logService")
public class LogServiceImpl {
	
	private static Logger logger=Logger.getLogger(LogServiceImpl.class);
	
	public String writeToLog(String id){
		
		logger.info("Writing something on this log: "+id);
		
		return "Go check the log";
		
	}

}
