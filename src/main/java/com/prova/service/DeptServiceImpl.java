package com.prova.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prova.api.DeptItem;
import com.prova.dao.DeptDao;
import com.prova.exceptions.ProvaException;

@Service("deptService")
public class DeptServiceImpl {
	
	@Autowired
	DeptDao deptDao;
	
	public DeptItem getDept(String id) throws ProvaException{
		
		DeptItem deptItem = deptDao.selectById(id);
		
		return deptItem;
		
	}

	public String addDept(DeptItem item) {
		
		deptDao.insert(item);
		
		return "Inserted "+item.toString();
		
	}
	
}
