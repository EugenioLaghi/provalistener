package com.prova.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.beans.factory.annotation.Autowired;

import com.prova.response.SessionItem;

//import com.prova.response.ProvaResponse;

//@Aspect
public class AspectProva {

//	@Autowired
	SessionItem sessionItem;

//	public AspectProva(){
//		
//	}
	
//	@Pointcut("execution(* com.prova.controller.*.*(..))")
	public void executeOperations() {
		System.out.println("@@@@@@@@@@@@@@@@ execution out @@@@@@@@@@@@@@@@@@@@@@@");
		sessionItem.setParam("execute");
	}
	
//	@Around("executeOperations()")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
//		long linizio = System.currentTimeMillis();
		try {
			// ****************************************************************************
			// LOG ANY INPUT
			// ****************************************************************************
			// ****************************************************************************
			// LOG OUTPUT
			// ****************************************************************************
//			ProvaResponse<?> result = (ProvaResponse<?>) joinPoint.proceed();
			System.out.println("@@@@@@@@@@@@@@@@ execution in @@@@@@@@@@@@@@@@@@@@@@@");
			Object result = joinPoint.proceed();
//			throw new Exception();
//			result.setEndTime(System.currentTimeMillis());
//			result.setParam("1");
			
			return result;

		} catch (Throwable ex) {
			throw ex;
		}
	}
	
}
